# face_recognition

Face recognition project with face-recognizer and dockerizing the project.

1. Check if docker is installed => docker --version
2. run `docker build -t name . `
3. check for docker images and run the build.

To run locally

1. pip3 install -r requirements.txt
2. run in terminal => uvicorn app:app --host 0.0.0.0 --port 8000 --reload
3. Visit 0.0.0.0:8000/docs to test your webapp for face recognition.
