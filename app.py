import face_recognition
from fastapi import FastAPI, File, UploadFile
import os

app = FastAPI()

@app.get('/')
def index():
    return {"Hello, world":"satya"}


@app.post("/upload-file/")
async def upload_source_file(src_file: UploadFile = File(...), tar_file: UploadFile = File(...)):
    
    file_src = f"img/{src_file.filename}"
    file_tar = f"img/{tar_file.filename}"

    with open(file_src, "wb+") as file_object:
        file_object.write(src_file.file.read())

    with open(file_tar, "wb+") as file_object:
        file_object.write(tar_file.file.read())

    return {"upload_complete": True}

    # return {"info": f"file '{uploaded_file.filename}' saved at '{file_location}'"}

@app.get("/get_res")
def is_matched():
    f1,f2 = os.listdir("img/")
    f1, f2 = os.path.join("img", f1), os.path.join("img", f2)
    file1 = face_recognition.load_image_file(f1)
    file2 = face_recognition.load_image_file(f2)

    f1_encoding = face_recognition.face_encodings(file1)[0]
    f2_encoding = face_recognition.face_encodings(file2)[0]

    res = face_recognition.compare_faces([f1_encoding], f2_encoding)

    # os.system("rm -rf img/*")

    state = "false"
    if any(res):
        state="true"

    return {"result": state}


# @app.post("/upload-target-file/")
# async def upload_target_file(tar_file: UploadFile = File(...)):
#     file_location = f"img/{uploaded_file.filename}"
#     with open(file_location, "wb+") as file_object:
#         file_object.write(uploaded_file.file.read())
#     return {"info": f"file '{uploaded_file.filename}' saved at '{file_location}'"}

# @app.get('/get_result')
# def get_result():
#     file1 = os.path.join('img', os.listdir('img')[0])
#     file2 = os.path.join('img', os.listdir('img')[1])

#     file1 = face_recognition.load_image_file(file1)
#     file2 = face_recognition.load_image_file(file2)

#     file1_encoding = face_recognition.face_encodings(file1)[0]
#     file2_encoding = face_recognition.face_encodings(file2)[0]

#     res = face_recognition.compare_faces([file1_encoding], file2_encoding)[0]

#     return {"matched": res}