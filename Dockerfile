FROM python:3

RUN apt-get update -y
RUN apt install vim -y
RUN apt-get install build-essential cmake -y

COPY requirements.txt /tmp/requirements.txt
RUN python3 -m pip install -r /tmp/requirements.txt

EXPOSE 8002
CMD ["uvicorn", "app:app", "--host", "0.0.0.0", "--port", "8002", "--workers", "2"]
